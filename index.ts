import { Request, Response, json, static as serveStatic } from 'express';
import { AzamornOVH } from '../../src/main';
import { Service, Module } from '../index';
import Joi = require('@hapi/joi');

const path = require('path');
const md5 = require('crypto-js/md5');
const dotenv = require('dotenv').config({
    path: path.join(__dirname, '.env')
    //Allows the .env file to be loaded relative to the module directory
});
const GCSTextToSpeech = require('@google-cloud/text-to-speech');
const gcs_tts = new GCSTextToSpeech.TextToSpeechClient({
    keyFilename: path.join(__dirname, dotenv.parsed.GOOGLE_APPLICATION_CREDENTIALS)
    //Allows the credentials json to be loaded relative to the module directory
});
const rateLimit = require("express-rate-limit");
const voice = { languageCode: 'en-AU', name: `en-AU-Wavenet-D`, ssmlGender: 'MALE' } //Awesome voice!
const textToSpeech = (text) => {
    return new Promise((resolve) => {
      gcs_tts.synthesizeSpeech({
        input: { text: text },
        voice: voice,
        audioConfig: { audioEncoding: 'MP3' },
      }).then((response) => {
        resolve(response[0].audioContent);
      })
    })
  }

export class TTSModule extends Module {
    config: any;
    tts_schema: any;
    app: AzamornOVH;
    cache_ttl: number = 60 * 60 * 24;
    limiter: any;
    constructor(app: AzamornOVH) {
        super("Text To Speech Module");
        this.app = app;
        this.config = dotenv.parsed;
        this.tts_schema = Joi.object({
            text: Joi.string().uri().required()
        });
        this.limiter = rateLimit({
            windowMs: 60 * 60 * 1000, // 1 hour
            max: 60, // limit each IP to 10 requests per windowMs
            handler: (req, res) => {
                res.status(200).json({ error: "Your hourly quota has been reached." })
            },
            skip: (req, res) => {
                const inputText = req.body.text;
                if (inputText.length > 0 && inputText.length <= 100) {
                    const cacheKey = md5(inputText).toString(),
                        cachedAudio = this.app.cache.get(cacheKey);
                    if (cachedAudio == undefined) {
                        return false; //return false only if the data is coming from Google Cloud
                    }
                }
                return true;
            }
        });
        return this;
    }
    init(): Promise<void> {
        return new Promise((resolve) => {
            this.app.web.get('/tts', (req: Request, res: Response) => {
                res.sendFile(path.resolve(path.join(__dirname, 'html', 'index.html')));
            });
            this.app.api.post('/tts', json(), (req: Request, res: Response) => {
                const inputText = req.body.text;
                if (inputText.length > 0 && inputText.length <= 100) {
                    const cacheKey = md5(inputText).toString(),
                        cachedAudio = this.app.cache.get(cacheKey);
                    if (cachedAudio == undefined) {
                        textToSpeech(inputText).then((audio : Buffer) => {
                            this.app.cache.set(cacheKey, audio, this.cache_ttl);
                            res.status(200).json({ audio: audio.toString('base64') })
                        }).catch((err) => {
                            console.error(err);
                            res.status(200).json({ error: "GCS returned an error, please try again later." })
                        });
                    }
                    else {
                        res.status(200).json({ audio: (cachedAudio as Buffer).toString('base64') })
                    }
                }
                if (inputText.length == 0) {
                    res.status(200).json({ error: "Please enter a sentence to be synthesized to speech." })
                }
                else if (inputText.length > 100) {
                    res.status(200).json({ error: "Only up to 100 characters are allowed at a time." })
                }
            });
            this.app.web.use('/resources/tts-module', serveStatic(path.resolve(path.join(__dirname, 'resources'))))
            resolve();
        })
    }
    unique_id() {
        return Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5)
    }
}
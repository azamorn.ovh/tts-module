import React, { Component } from 'react';
import { Modal } from './modal';
const axios = require('axios');

export class App extends Component {

    gcs_logo : string = "https://www.gstatic.com/devrel-devsite/prod/va2f579f943e40687d02fe75a771878e054c901286ea550f8e49c5efb402dac68/cloud/images/cloud-logo.svg";
    input : any;
    audioRef : any;
    modalRef : any;
    state: any;

    constructor(props: any) {
        super(props);
        this.state = {value: '',busy:false};
        this.audioRef = React.createRef();
        this.modalRef = React.createRef();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.showError = this.showError.bind(this);
    }

    componentDidMount(){
        this.audioRef.current.onended = this.audioEnded.bind(this);
    }

    audioEnded(){
        this.setState({
            busy : false
        });
    }

    showError(error : string){
        this.modalRef.current.showError(error);
    }

    handleChange(event: any){
        this.setState({value: event.target.value});
    }

    handleSubmit(event : any){
        event.preventDefault();
        let audio = this.audioRef.current;
        this.setState({ busy:true }, () => {
            axios.post('https://api.azamorn.ovh/tts',{text: this.state.value})
            .then((res : any) => {
                if(res.data.audio != undefined){
                    audio.src = `data:audio/mp3;base64,${res.data.audio}`;
                    audio.play();
                }
                else{
                    this.showError(res.data.error);
                }
            });
        });
        
    }

    render() {
        return (
            <div className="page">
                <Modal ref={this.modalRef} />
                <form className="tts-react" onSubmit={this.handleSubmit}>
                    <p className="lead text-center mb-30">Type anything and Google's Text-To-Speech will convert it into Audio</p>
                    <div className="input-group mb-3">
                        <input ref={this.input} id="text-input" max="101" type="text" className="form-control" onChange={this.handleChange}/>
                        <div className="input-group-append">
                            <button className={`btn btn-primary${this.state.busy ? ' disabled' : ''}`} type="submit">Say this!</button>
                        </div>
                    </div>
                    <div className="powered-by">
                        <p className="lead">Powered by <img src={this.gcs_logo} className="google-cloud" /></p>
                    </div>
                    <audio ref={this.audioRef} id="tts-audio"></audio>
                </form>
            </div>
        )
    }
}
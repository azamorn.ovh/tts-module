import React, { Component } from 'react';
const axios = require('axios');

export class Modal extends Component {
    
    state : any;
    modalRef : any;

    constructor (props: any){
        super(props);
        this.showError = this.showError.bind(this);
        this.state = {error : ""}
        this.modalRef = React.createRef();
    }
    
    showError(error : string){
        this.setState({
            error: error
        }, () => {
            //Let bootstrap take care of showing the modal
            ($('#modal') as any).modal('show');
        })
    }

    render() {
        return (
            <div id="modal" className="modal fade" data-backdrop="static"
                aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Error</h5>
                        </div>
                        <div className="modal-body">{ this.state.error }</div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Okay</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

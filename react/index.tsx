import { App } from './app';
import React from 'react';
import ReactDOM from 'react-dom';

const wrapper = document.getElementById("page-container");
ReactDOM.render(<App />, wrapper)